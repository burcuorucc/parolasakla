parolalar = [] #parolalar için bir dizin oluşturduk
while True:  #sonsuz bir döngüye soktuk
    print('Bir işlem seçin') #alttaki 2 satırda yapacagimiz islemi secmek icin bir menü olusturduk
    print('1- Parolaları Listele')
    print('2- Yeni Parola Kaydet')
    islem = input('Ne Yapmak İstiyorsun :')#kullanicidan bir secim yapmasini istedik
    if islem.isdigit(): #burada kullanıcının girdigi deger bir sayi mi kontrol ettik duruma göre döngüye girdik
        islem_int = int(islem) #aldigimiz degeri bir sayi ise bunu islem_inte atadık
        if islem_int not in [1, 2]: #girilen sayi 1 ya da 2 degilse hata yazisini bir alt satırda ekrana yazdirdik
            print('Hatalı işlem girişi')
            continue #döngüden cikip menüye geri dönmeyi sagladik
        if islem_int == 2: #kullanicinin girdigi deger 2 ise alttaki 6 satir boyunca kullanicidan gerekli bilgileri aldik

            girdi_ismi = input('Bir girdi ismi ya da web sitesi adresi girin :')

            kullanici_adi = input('Kullanici Adi Girin :')

            parola = input('Parola :')
            parola2 = input('Parola Yeniden :')  #parolanın dogru girip girilmedigini test etmek icin teyit aldik
            eposta = input('Kayitli E-posta :')
            gizlisorucevabi = input('Gizli Soru Cevabı :')
            if kullanici_adi.strip() == '':  #bu ve alttaki 17 satirda kullanicidan istedigimiz bu deger bos birakilmissa bir alttaki satirda kullaniciya hatasini bildirdik
                print('kullanici_adi girmediz')
                continue  # donguden cikip kullaniciya hatasini en alttaki print sayesinde bildirdik
                 if parola.strip() == '':
                print('parola girmediz')
                continue
            if parola2.strip() == '':
                print('parola2 girmediz')
                continue
            if eposta.strip() == '':
                print('eposta girmediz')
                continue
            if gizlisorucevabi.strip() == '':
                print('gizlisorucevabi girmediz')
                continue
            if girdi_ismi.strip() == '':
                print('Girdi ismi girmediz')
                continue
            if parola2 != parola:
                print('Parolalar eşit değil') #teyit etmek icin istedigimiz iki parola esit degilse ekrana hatayi yazdirdik
                continue

            yeni_girdi = {   #almis oldugumuz girdileri burada yeni girdi olarak tek bir dizi olarak atadik
                'girdi_ismi': girdi_ismi,
                'kullanici_adi': kullanici_adi,
                'parola': parola,
                'eposta': eposta,
                'gizlisorucevabi': gizlisorucevabi,
            }
            parolalar.append(yeni_girdi) #parolalara atadigimiz bu yeni girdiyi append ile ekledik
            continue #döngüden cikip menuye donduk
        elif islem_int == 1: #burada kullanici parolalarini listelemek istiyor demektir
            alt_islem_parola_no = 0 #parolaların 0dan baslayip sırali olarak dizilmesi icin ilk olarak 0a atadik
            for parola in parolalar: #parola kelimesi gectikce for döngüsüne girdik
                alt_islem_parola_no += 1  #her parola kelimesi icin parola nosunu bir arttirdik
                print('{parola_no} - {girdi}'.format(parola_no=alt_islem_parola_no, girdi=parola.get('girdi_ismi'))) #önceden girilmis olan parola ve parola noları format ve get sayesinde cagrilmis ve dizilmis oldu
            alt_islem = input('Yukarıdakilerden hangisi ?: ') #kullanicidan hangi parola ile calismak istedigimizi istedik
            if alt_islem.isdigit(): #kullanicidan aldigimiz deger bir sayiysa döngüye girdik,digit ile sorguladik
                if int(alt_islem) < 1 and len(parolalar) - 1 < int(alt_islem): #kullanıcıdan aldigimiz degerin listedeki sayilarda var olup olmadigini kontrol ettik
                    print('Hatalı parola seçimi') #hataliysa kullaniciya bildirdik
                    continue #yeni bir deger almak icin geri döndük
                parola = parolalar[int(alt_islem) - 1] #parola no dogru ise bilgileri alttaki 5 satirla birlikte kullaniciya bilgileri göstermek icin ekrana yazdirdik
                print('{kullanici}\n{parola}\n{gizli}\n{eposta}'.format( #\n ile bilgilerin alt alta yazilmasini sagladik,format sayesinde bu kayitli bilgileri buraya getirdik
                    kullanici=parola.get('kullanici_adi'), #get ile kullanici girmis oldugu degerleri görmüs oldu
                    parola=parola.get('parola'),
                    eposta=parola.get('eposta'),
                    gizli=parola.get('gizlisorucevabi'),
                ))
                continue #döngüden cikip yeni bir islem icin kullanicidan hangi islemi yapmak istedigini sorduk

    print('Hatalı giriş yaptınız') #eger ilk durum true degilse menu bile acilmadan hata bilgisini kullaniciya gösterdik
