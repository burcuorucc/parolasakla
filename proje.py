"""
isimler= ["Alper","Melda","Ferdi",
          "Sinem","Oguzhan","Zehra","Emre","Ugurcan",
          "Dogukan","Hilal","Diren","Mehmet","Mertcan","Ali",
          "Ferhat","Cem","Gokhan","Sezer","Nur","Busra","Pelin",
          "Kadir","Muhammet","Ture","Burcu","Damla",
         ]
#isimler.remove("Alper")
#print((isimler))

# print((isimler.pop(2))) cikarma
print(isimler)
# isimler.append(2)  ekleme
print(isimler)
print(isimler.index("Kadir"))
print(isimler.count("Alper")) #sayma
isimler2=isimler.copy()
yenigelenler=['Tony','Ismail']
isimler2.append((yenigelenler)) #liste halinde ekler,tek eleman gibi
print(isimler2)
isimler.extend(yenigelenler) # tek tek ekler
print(isimler)
isimler2.clear() #listenin içini temizler
isimler.sort() # sıralama,karakter ya da int deger olabilir
print(isimler)
isimler.reverse() #ters sıralama
isimler2.sort(reverse=True)
print(isimler)
isimler.insert(5,"Mustafa")
print(isimler)


ogrenci={"isim":"Tolga","soyisim":"ozeren"}
print(ogrenci.get("isim"))
print(ogrenci["isim"]) #koseli parantez ile de cagrılabilir

print(ogrenci.get("yas",0))
ogrenci.update({"yas":"22"}) #yeni key value ekleme ya da degistirmek icin
print(ogrenci)
ogrenci["yas"]=23 #değeri değiştirmek update etmek icin
print(ogrenci)
print(ogrenci.items())
for key,value in ogrenci.items():
    print(key,value)
for item in ogrenci.items():
    print(item[0],item[1])


makine = {"name":"m80",
          "device_status":{
              "device_response_status":"run",
              "calculated": {
                  "device_maintance":5,"device_error":0
              }
          }
          }
print(makine.get("device_status").get("device_response_status"))
print(type(makine.get("device_status")))

ogrenci={"numara":206,"notlar":[{"ders_adi":"matematik","not":100},
                                {"ders_adi":"edebiyat","not":0}
         ]
         }
print(ogrenci.keys())
print(ogrenci.fromkeys(["notlar"]))
#ogrenci=dict.fromkeys(["notlar","numara","isim"],"girilmemis")
print(ogrenci)

for value in ogrenci.values():
    print(value)

ogrenci.pop("notlar") #tüm notları siler
print(ogrenci)

"""
kacdefa=0
while kacdefa < 3:
    sayi=input("Bana bir sayi ver:")
    print("kullanıcının yazgidi:",sayi)
    if sayi.isdigit():
            if int(sayi)== 13:
                print("13le 13ü carpamazsin")
                continue
            carpim = int (sayi)* 13
            print("Kullanıcının yazdigi 13 ile carpilinca:",carpim)
            kacdefa +=1

print(kacdefa)
if kacdefa==3:
    print("cok kez yanlis girdiniz")


sayi =0
while sayi < 100:
    print("Sayi:",sayi)
    sayi+=1

